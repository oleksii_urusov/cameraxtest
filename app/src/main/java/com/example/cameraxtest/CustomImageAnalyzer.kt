package com.example.cameraxtest

import android.util.Log
import android.widget.TextView
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

class CustomImageAnalyzer(private val outputView: TextView, lifecycle: Lifecycle) : ImageAnalysis.Analyzer,
    LifecycleObserver {
    private var isActive: Boolean = false
    private var lastAnalyzedTimestamp = 0L

    init {
        lifecycle.addObserver(this)
    }

    override fun analyze(image: ImageProxy?, rotationDegrees: Int) {
        if (!isActive) return

        val currentTimestamp = System.currentTimeMillis()
        // Calculate the average luma no more often than every second
        if (currentTimestamp - lastAnalyzedTimestamp >=
            TimeUnit.SECONDS.toMillis(1)
        ) {
            // Since format in ImageAnalysis is YUV, image.planes[0]
            // contains the Y (luminance) plane
            val buffer = image?.planes?.get(0)?.buffer ?: return
            // Extract image data from callback object
            val data = buffer.toByteArray()
            // Convert the data into an array of pixel values
            val pixels = data.map { it.toInt() and 0xFF }
            // Compute average luminance for the image
            val luma = pixels.average()
            val lumaFormatted = "%.2f".format(luma)
            // Log the new luma value
            Log.d("CameraXTest", "Average luminosity: $lumaFormatted")
            outputView.post {
                outputView.text = "Average luminosity:\n $lumaFormatted"
            }
            // Update timestamp of last analyzed frame
            lastAnalyzedTimestamp = currentTimestamp
        }
    }

    private fun ByteBuffer.toByteArray(): ByteArray {
        rewind() // Rewind the buffer to zero
        val data = ByteArray(remaining())
        get(data) // Copy the buffer into a byte array
        return data // Return the byte array
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        isActive = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        isActive = false
    }
}