package com.example.cameraxtest

import android.Manifest
import android.os.Bundle
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import com.example.cameraxtest.util.CameraHelper
import com.example.cameraxtest.util.PermissionHandler
import com.example.cameraxtest.util.showToast
import com.example.cameraxtest.util.toGone
import com.example.cameraxtest.util.toVisible
import kotlinx.android.synthetic.main.activity_main.anlysis_output as analysisOutputView
import kotlinx.android.synthetic.main.activity_main.capture_button as captureButton
import kotlinx.android.synthetic.main.activity_main.camera_root_view as rootView
import kotlinx.android.synthetic.main.activity_main.view_finder as viewFinder


class MainActivity : AppCompatActivity() {
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private val permissionHandler = PermissionHandler(this)
    private lateinit var cameraHelper: CameraHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFullScreen()
        setUpViews()
        launchCamera()
    }

    private fun setUpViews() {
        captureButton.setOnClickListener { cameraHelper.takePicture() }
    }

    private fun setFullScreen() {
        rootView.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE or
                SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

        ViewCompat.setOnApplyWindowInsetsListener(captureButton) { view, insets ->
            view.updateLayoutParams<ViewGroup.MarginLayoutParams> { bottomMargin = insets.systemWindowInsetBottom }
            insets
        }
    }

    private fun launchCamera() {
        cameraHelper = CameraHelper(this, rootView, viewFinder, analysisOutputView)
        permissionHandler.withPermissions(REQUIRED_PERMISSIONS) { permissionsGranted ->
            if (permissionsGranted) {
                captureButton.toVisible()
                cameraHelper.launchCamera()
            } else {
                captureButton.toGone()
                showToast("Permissions not granted by the user.")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHandler.onRequestPermissionResult(requestCode, permissions, grantResults)
    }
}