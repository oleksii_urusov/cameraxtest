package com.example.cameraxtest.listener

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.camera.core.ImageCapture
import java.io.File

class ImageSavedListener(private val context: Context) : ImageCapture.OnImageSavedListener {

    override fun onError(error: ImageCapture.UseCaseError,
                         message: String, exc: Throwable?) {
        val msg = "Photo capture failed: $message"
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        Log.e("CameraXTest", msg)
        exc?.printStackTrace()
    }

    override fun onImageSaved(file: File) {
        val msg = "Photo capture succeeded: ${file.absolutePath}"
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        Log.d("CameraXTest", msg)
    }
}