package com.example.cameraxtest.listener

import android.hardware.display.DisplayManager

class OnDisplayChangeListener(private val onDisplayChange: () -> Unit) : DisplayManager.DisplayListener {
    override fun onDisplayAdded(displayId: Int) = Unit
    override fun onDisplayRemoved(displayId: Int) = Unit
    override fun onDisplayChanged(displayId: Int) = onDisplayChange()
}