package com.example.cameraxtest.util

import android.os.Handler
import android.os.HandlerThread
import android.widget.TextView
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageAnalysisConfig
import androidx.lifecycle.Lifecycle
import com.example.cameraxtest.CustomImageAnalyzer

class AnalyzerHelper(analyzeResultsOutputView: TextView, lifecycle: Lifecycle) {
    val useCase: ImageAnalysis

    init {
        // Setup image analysis pipeline that computes average pixel luminance
        val analyzerConfig = ImageAnalysisConfig.Builder().apply {
            // Use a worker thread for image analysis to prevent glitches
            val analyzerThread = HandlerThread(
                "CustomImageAnalyzer").apply { start() }
            setCallbackHandler(Handler(analyzerThread.looper))
            // In our analysis, we care more about the latest image than
            // analyzing *every* image
            setImageReaderMode(
                ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        }.build()

        // Build the image analysis use case and instantiate our analyzer
        useCase = ImageAnalysis(analyzerConfig).apply {
            analyzer = CustomImageAnalyzer(analyzeResultsOutputView, lifecycle)
        }
    }

    fun setTargetRotation(rotation: Int) {
        useCase.setTargetRotation(rotation)
    }
}