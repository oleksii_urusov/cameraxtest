package com.example.cameraxtest.util

import android.content.Context
import android.hardware.display.DisplayManager
import android.util.DisplayMetrics
import android.util.Log
import android.util.Rational
import android.view.TextureView
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraX
import androidx.camera.core.Preview
import androidx.camera.core.PreviewConfig
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.cameraxtest.listener.ImageSavedListener
import com.example.cameraxtest.listener.OnDisplayChangeListener
import java.io.File

private val TAG = "CameraHelper"

class CameraHelper(
    private val activity: AppCompatActivity,
    private val rootView: ViewGroup,
    private val viewFinder: TextureView,
    private val analyzeResultsOutputView: TextView
) : LifecycleObserver {

    private var displayId: Int = -1
    private lateinit var displayManager: DisplayManager

    private var preview: Preview? = null
    private var imageCaptureHelper: ImageCaptureHelper? = null
    private var analyzerHelper: AnalyzerHelper? = null

    private var isActive: Boolean = false

    private val aspectRatio: Rational
        get() = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }.let {
            Rational(
                it.widthPixels,
                it.heightPixels
            )
        }

    private val displayListener = OnDisplayChangeListener {
        if (displayId == this@CameraHelper.displayId) {
            Log.d(TAG, "Rotation changed: ${rootView.display.rotation}")
            preview?.setTargetRotation(rootView.display.rotation)
            imageCaptureHelper?.setTargetRotation(rootView.display.rotation)
            analyzerHelper?.setTargetRotation(rootView.display.rotation)
        }
    }

    init {
        activity.lifecycle.addObserver(this)
    }

    fun launchCamera() {
        viewFinder.post {
            displayId = viewFinder.display.displayId
            setUpPreview()
            imageCaptureHelper = ImageCaptureHelper(aspectRatio, viewFinder)
            analyzerHelper = AnalyzerHelper(analyzeResultsOutputView, activity.lifecycle)
            CameraX.bindToLifecycle(activity, preview, imageCaptureHelper?.useCase, analyzerHelper?.useCase)
        }
    }

    fun takePicture() {
        val file = File(
            activity.externalMediaDirs.first(),
            "${System.currentTimeMillis()}.jpg"
        )
        imageCaptureHelper?.takePicture(file, ImageSavedListener(activity))
    }

    private fun setUpPreview() {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetAspectRatio(aspectRatio)
            setLensFacing(CameraX.LensFacing.BACK)
            setTargetRotation(viewFinder.display.rotation)
        }.build()
        preview = Preview(previewConfig).also {
            setUpTransformationHandler(it)
        }
    }

    private fun setUpTransformationHandler(preview: Preview) {
        TransformationHandler(viewFinder, preview).onChange { matrix ->
            if (!isActive) return@onChange
            matrix?.let { viewFinder.setTransform(it) }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun onCreate() {
        displayManager = activity.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        displayManager.registerDisplayListener(displayListener, null)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        isActive = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        isActive = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        displayManager.unregisterDisplayListener(displayListener)
    }
}