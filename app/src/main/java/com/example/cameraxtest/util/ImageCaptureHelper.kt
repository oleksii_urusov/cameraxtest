package com.example.cameraxtest.util

import android.util.Rational
import android.view.TextureView
import androidx.camera.core.CameraX
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureConfig
import com.example.cameraxtest.listener.ImageSavedListener
import java.io.File

class ImageCaptureHelper(aspectRatio: Rational, viewFinder: TextureView) {
    val useCase: ImageCapture

    init {
        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setTargetAspectRatio(aspectRatio)
            setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            setTargetRotation(viewFinder.display.rotation)
            setLensFacing(CameraX.LensFacing.BACK)
        }.build()
        useCase = ImageCapture(imageCaptureConfig)
    }

    fun setTargetRotation(rotation: Int) {
        useCase.setTargetRotation(rotation)
    }

    fun takePicture(file: File, imageSavedListener: ImageSavedListener) {
        useCase.takePicture(file, imageSavedListener)
    }
}