package com.example.cameraxtest.util

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class PermissionHandler(private val activity: Activity) {
    private var requestCode = 0
    private val permissionChecks = mutableMapOf<Int, PermissionCheck>()

    fun withPermissions(permissions: Array<String>, action: (permissionsGranted: Boolean) -> Unit) {
        if (allPermissionsGranted(permissions)) {
            action(true)
        } else {
            permissionChecks[requestCode] =
                PermissionCheck(requestCode, permissions, action)
            ActivityCompat.requestPermissions(
                activity, permissions, requestCode
            )
            requestCode++
        }
    }

    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        val permissionCheck = permissionChecks[requestCode] ?: return

        if (allPermissionsGranted(permissionCheck.permissions)) {
            permissionCheck.action(true)
        } else {
            permissionCheck.action(false)
        }
        permissionChecks -= requestCode
    }

    private fun allPermissionsGranted(permissions: Array<String>) = permissions.all { permission ->
        ContextCompat.checkSelfPermission(
            activity,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }
}

private class PermissionCheck(
    val requestCode: Int,
    val permissions: Array<String>,
    val action: (permissionsGranted: Boolean) -> Unit
)