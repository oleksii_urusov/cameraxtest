package com.example.cameraxtest.util

import android.graphics.Matrix
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import androidx.camera.core.Preview
import kotlin.math.roundToInt

private val TAG = "TransformationHandler"

class TransformationHandler(private val viewFinder: TextureView, preview: Preview) {
    private var bufferDimens = Size(1, 1)
    private var viewFinderDimens = Size(1, 1)
    private var onUpdateListener: ((Matrix?) -> Unit)? = null

    init {
        viewFinder.addOnLayoutChangeListener { _, left, top, right, bottom, _, _, _, _ ->
            viewFinderDimens = Size(right - left, bottom - top)
            Log.d(TAG, "View finder layout changed. Size: $viewFinderDimens")
            update()
        }

        preview.setOnPreviewOutputUpdateListener { previewOutput ->
            updateViewFinder(previewOutput)
            bufferDimens = previewOutput.textureSize
            update()
        }
    }

    fun onChange(listener: (Matrix?) -> Unit) {
        this.onUpdateListener = listener
    }

    private fun updateViewFinder(previewOutput: Preview.PreviewOutput) {
        (viewFinder.parent as ViewGroup).run {
            removeView(viewFinder)
            addView(viewFinder, 0)
        }
        viewFinder.surfaceTexture = previewOutput.surfaceTexture
    }

    private fun getTransformMatrix(): Matrix? {
        val matrix = Matrix()

        val rotationDegrees = when (viewFinder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return null
        }

        val centerX = viewFinder.width / 2f
        val centerY = viewFinder.height / 2f

        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        // Buffers are rotated relative to the device's 'natural' orientation: swap width and height
        val bufferRatio = bufferDimens.height / bufferDimens.width.toFloat()

        val scaledWidth: Int
        val scaledHeight: Int
        // Match longest sides together -- i.e. apply center-crop transformation
        if (viewFinderDimens.width > viewFinderDimens.height) {
            scaledHeight = viewFinderDimens.width
            scaledWidth = (viewFinderDimens.width * bufferRatio).roundToInt()
        } else {
            scaledHeight = viewFinderDimens.height
            scaledWidth = (viewFinderDimens.height * bufferRatio).roundToInt()
        }

        // Compute the relative scale value
        val xScale = scaledWidth / viewFinderDimens.width.toFloat()
        val yScale = scaledHeight / viewFinderDimens.height.toFloat()

        // Scale input buffers to fill the view finder
        matrix.preScale(xScale, yScale, centerX, centerY)
        return matrix
    }

    private fun update() {
        onUpdateListener?.let { it(getTransformMatrix()) }
    }
}